function select (dataSet, { id, auto, minPlayTime, merge } = {} ) {
  const isIdDefined = !!id;
  const isAutoDefined = auto === true || auto === false;
  const isMinPlayTimeDefined = !!minPlayTime || minPlayTime === 0;
  const isMergeDefined = !!merge;

  const results = [];
  const indexMemo = {};

  for (let index = 0, dataSetLength = dataSet.length; index < dataSetLength; index++) {
    const item = dataSet[index];

    if (isIdDefined && item.id !== id) {
      continue
    }
    if (isAutoDefined && item.auto !== auto) {
      continue
    }
    if (isMinPlayTimeDefined && item.playTime < minPlayTime) {
      continue
    }

    if (isMergeDefined) {
      if (indexMemo[item.id] > -1) {
        results[indexMemo[item.id]].playTime += item.playTime;
        if (item.auto === false) {
          results[indexMemo[item.id]].auto = false
        }
        continue
      } else {
        indexMemo[item.id] = results.length;
      }
    }

    results.push({ ...item });
  }

  return results;
}

module.exports = select;
