const expect = require('chai').expect;
const select = require('./index');
const dataSet = require('./sample-data.json');

const realId = 118748738;
const realIdString = '' + realId;

const getDistinctValuesOfProperties = (data, property) => {
  const uniqueValues = {};
  data.forEach(element => uniqueValues[element[property]] = true)

  return Object.keys(uniqueValues);
}

describe('select function', () => {
  describe('when passing empty data array', () => {
    const emptyData = [];
    describe('with defined options', () => {
      it('should return empty array', () => {
        expect(select(emptyData, { id: realId })).to.deep.equal([]);
      })
    })

    describe('without defined options', () => {
      it('should return empty array', () => {
        expect(select(emptyData)).to.deep.equal([]);
      })
    })
  })


  describe('when passing real data', () => {
    const testCases = [
      { name: 'id', options: { id: realId }, count: 6, test: (data) => {
        expect(getDistinctValuesOfProperties(data, 'id')).to.deep.equal([ realIdString ]);
      }},
      { name: 'auto = true',  options: { auto: true }, count: 10, test: (data) => {
        expect(getDistinctValuesOfProperties(data, 'auto')).to.deep.equal([ 'true' ]);
      }},
      { name: 'auto = false', options: { auto: false }, count: 40, test: (data) => {
        expect(getDistinctValuesOfProperties(data, 'auto')).to.deep.equal([ 'false' ]);
      }},
      { name: 'minPlayTime', options: { minPlayTime: 1000000 }, count: 1, test: (data) => {
        expect(getDistinctValuesOfProperties(data, 'playTime')).to.deep.equal([ '2033206' ]);
      }},
      { name: 'combined id and auto', options: { id: realId, auto: true }, count: 2, test: (data) => {
        expect(getDistinctValuesOfProperties(data, 'id')).to.deep.equal([ realIdString ]);
        expect(getDistinctValuesOfProperties(data, 'auto')).to.deep.equal([ 'true' ]);
      }},
      { name: 'combined id and minPlayTime', options: { id: realId, minPlayTime: 5000 }, count: 3, test: (data) => {
        expect(getDistinctValuesOfProperties(data, 'id')).to.deep.equal([ realIdString ]);
        expect(getDistinctValuesOfProperties(data, 'playTime')).to.deep.equal([ '5113', '10609', '12002' ]);
      }},
      { name: 'merge = true and id', options: { id: realId, merge: true }, count: 1, test: (data) => {
        expect(getDistinctValuesOfProperties(data, 'id')).to.deep.equal([ realIdString ]);
        expect(getDistinctValuesOfProperties(data, 'auto')).to.deep.equal([ 'false' ]);
      }},
      { name: 'nothing', options: {}, count: 50 },
      { name: 'undefined', options: undefined, count: 50 }
    ]

    testCases.forEach((testCase) => {
      const { name, options, count, test } = testCase
      describe(`with ${name} in options`, () => {
        const data = select(dataSet, options);

        it('should return correct number of elements', () => {
          expect(data).to.have.lengthOf(count);
        })

        it('should return only elements with correct values', () => {
          test && test(data);
        })
      })
    })

    it('does not modify the source data', () => {
      const originalData = require('./sample-data.json');

      select(dataSet, { id: realId, minPlayTime: 5000 });
      expect(dataSet).to.deep.equal(originalData);
    })
  })
})
