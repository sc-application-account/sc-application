## General Notes
The main algoritm can be found in `index.js` and the tests are in `index.test.js`. The tests are using data from `sample-data.json`

To run test type in terminal `npm install && npm run test`

* In `index.js` there is only function definition + export, the usage examples are included in test cases.
* The libraries from `package.json` are used only to run the tests.
* I was considering using bitwise XOR instead of `!==` as generally it should be faster but it didn't increase the measurable performance and would just obfuscate the code.