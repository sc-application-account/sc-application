(function main() {

  const longWaveformCanvas = document.getElementById('waveform-long');
  const longSoundPlayButton = document.getElementById('play-button-long');
  const shortWaveformCanvas = document.getElementById('waveform-short');
  const shortSoundPlayButton = document.getElementById('play-button-short');

  const longSound = new SC.Sound(SC.longSound);
  const shortSound = new SC.Sound(SC.shortSound);

  const longWaveform = new SC.Waveform({
    canvas: longWaveformCanvas,
    sound: longSound
  });

  const shortWaveform = new SC.Waveform({
    canvas: shortWaveformCanvas,
    sound: shortSound
  });

  longWaveform.render();
  shortWaveform.render();

  longWaveformCanvas.addEventListener('mousedown', (event) => {
    const onMouseMove = (event) => {
      const { offsetX } = event
      const time = offsetX / longWaveformCanvas.width * longSound.duration
      longSound.seek(time)
    }

    onMouseMove(event)
    longWaveformCanvas.addEventListener('mousemove', onMouseMove)
    longWaveformCanvas.addEventListener('mouseup', () => longWaveformCanvas.removeEventListener('mousemove', onMouseMove))
  })

  shortWaveformCanvas.addEventListener('mousedown', (event) => {
    const onMouseMove = (event) => {
      const { offsetX } = event
      const time = offsetX / shortWaveformCanvas.width * shortSound.duration
      shortSound.seek(time)
    }

    onMouseMove(event)
    shortWaveformCanvas.addEventListener('mousemove', onMouseMove)
    shortWaveformCanvas.addEventListener('mouseup', () => shortWaveformCanvas.removeEventListener('mousemove', onMouseMove))
  })

  longSoundPlayButton.addEventListener('click', () => {
    longSound.toggle();
    longSoundPlayButton.classList.toggle('sc-button-play');
    longSoundPlayButton.classList.toggle('sc-button-pause');
  });

  shortSoundPlayButton.addEventListener('click', () => {
    shortSound.toggle();
    shortSoundPlayButton.classList.toggle('sc-button-play');
    shortSoundPlayButton.classList.toggle('sc-button-pause');
  });

})();
