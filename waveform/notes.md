## General Notes

* In my solution I have decided to change rerendering of the whole soundwave to rendering only the part that was changed from the last render. The comparison is made by taking the sampleInd which is currently played and comparing it to the last value played. In the first paint it draws everything.
Thanks to this approach my handler takes around 0.1 ms to handle a timer tick, down from 8-9 ms

* I have also added handling of seek functionality by adding listeners in `main.js` that then is simply handled by my comparison function in `waveform.js`

## My comments to the initial version of code:
* Clearing the whole canvas on every update and rerendering the whole soundwave every time

* Rerendering even when no visual change is happening, for the bigger file change is visible after every ~2000ms
This can be observed in Perfomence tab on Google Chrome DevTools with screenshots and call stack and in debugging tab by observing value of which sampleInd is currently being "played".

* Repetative computations, for example in 2nd inner loop (drawing vertical sound line) it evaluates
```
x < this.sound.currentTime / this.sound.duration * width ? '#f60' : '#333';
```
this value will not change throughout the loop but it is computed for every iteration increasing overall execution time

* A lot of variables defined in loops and used once in scope cause a major Garbage Collection (1.4MB) observed in Performence tab in DevTools

* Rendering one pixel at a time takes longer than rendering just one line
```
for (let y = value; y < height - value; y++) { ctx.fillRect(x, y, 1, 1); }
```

* This might have been ommitted on purpose but when the sound is finishing it does not switch the classes of the play button. I have not fixed this issue as it is rather out of scope of this task.

* Using interchangably `offsetWidth` and `width`, when offsetWidth includes border, padding and margin, while width does not
