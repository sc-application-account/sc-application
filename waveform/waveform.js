/**
 * A view of a waveform.
 */
SC.Waveform = (() => {

  class Waveform {

    constructor({ sound, canvas }) {
      this.sound = sound;
      this.canvas = canvas;
      this.canvasWidth = this.canvas.width;
      this.canvasHeight = this.canvas.height;

      this.pointer = -1; // which sample part is currently played

      this.sound.onTimeUpdate(() => { this.update() });
    }

    // Draw the canvas the first time. This is called once only, and before any
    // calls to `update()`.
    render() {
      this.paint(0, this.canvas.width, 0);
      this.pointer = 0;
    }

    paint(startingPoint, endingPoint, samplePointer) {
      const data = this.sound.waveformData;
      const ctx = this.canvas.getContext('2d');
      const widthScaling = data.width / this.canvasWidth;

      for (let x = startingPoint; x < endingPoint; x++) {
        const sampleInd = Math.floor(x * widthScaling);

        const value = Math.floor(
          this.canvasHeight * data.samples[sampleInd] / data.height / 2
        );

        ctx.fillStyle = x < samplePointer
          ? '#f60'
          : '#333';

        ctx.fillRect(x, value, 1, (this.canvasHeight - (2 * value)));
      }
    }

    // Update the visual state of the waveform so that it accurately represents
    // the play progress of its sound.
    update() {
      const ctx = this.canvas.getContext('2d');

      const samplePointer = Math.floor(this.sound.currentTime / this.sound.duration * this.canvas.width);

      if (this.canvasWidth !== this.canvas.width || this.canvasHeight !== this.canvas.height) {
        // if canvas was resized

        this.canvasWidth = this.canvas.width;
        this.canvasHeight = this.canvas.height;

        ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);

        this.paint(0, this.canvas.width, samplePointer);
      } else if (samplePointer > this.pointer) {
        // find part that requires redraw

        this.paint(this.pointer, samplePointer, samplePointer);
      } else if (samplePointer === this.pointer) {
        // if there is no change then ommit drawing

        return;
      } else if (samplePointer < this.pointer) {
        // find part that requires redraw

        this.paint(samplePointer, this.pointer, samplePointer);
      }

      this.pointer = samplePointer;
    }
  };

  return Waveform;

})();
