## General Notes
* I have changed regular download of images via src to fetch with assignment of result blob to src. This allows me to use AbortController to cancel requests on abort call.
* The average download speed of the big list on 10Mb/s connection is around 11 seconds now.
* I have also included a timeout for file downloading that take too much time to download to allow other images to download in the meantime.
This however increased the overall complexity of the code as for the initial run I am expecting to get a list of images, nulls (for 404s and network errors) and running requests (for big files or just slow downloads). Then I need to await for the remaining big images to download and filter out nulls.
