SC.loadImages = (() => {
  /**
   * Given an array of urls, load these images. When they are all loaded (or
   * errored), then resolve a deferred with an array of image elements ready to
   * be drawn onto the canvas.
   *
   * If the returned deferred object is rejected, we should stop loading these
   * images.
   *
   * @param {Array.<String>} urls
   * @return {Object}        Containing a promise with an array of images, as
   *                         well as an abort method which rejects the promise
   */

  const BATCH_SIZE = 10;

  function loadImagesBatch(urls) {
    return urls.map((url) => loadImage(url));
  }

  function loadImages(urls) {
    let rejectPromise;
    const startTime = Date.now();
    let imagesRequests = [];
    let imagesResults = [];
    let aborted = false;

    log(`Starting to load ${urls.length} images`);

    const imagesPromise = new Promise(async (resolve, reject) => {
      rejectPromise = reject;

      // To avoid downloading all images at once we can load them in batches of 10
      for(let index = 0, urlsLength = urls.length; index < urlsLength && !aborted; index += BATCH_SIZE) {
        imagesRequests = imagesRequests.concat(
          loadImagesBatch(urls.slice(index, index +BATCH_SIZE))
        );

        // First await all images promised to be either img, null or request (for long downloads)
        // The results are stored in imagesResults waiting for all data to finish downloading
        await Promise
          .all(imagesRequests.map(({ promise }) => promise))
          .then((results) => { imagesResults = results })
          .catch((error) => {
            // when Promises are aborted it will cause await Promise.all to fail
            if(error !== 'aborted') {
              throw error;
            }
          })
      }

      if(!aborted) {
        // pick out the Image elements from the resolution values of each
        // promise, and resolve the overall promise with these images.

        Promise
        .all(imagesResults.map(promisedImage => Promise.resolve(promisedImage.image.result)))
        .then(
          (finalImages) => {
            resolve(
              finalImages
                .map((finalImage) => finalImage && finalImage.result || finalImage)
                .filter(finalImage => Boolean(finalImage))
            )
          }
        )
      }
    });

    imagesPromise.then(
      () => { log(`Loading ${urls.length} took ${Date.now() - startTime}ms`) },
      () => { log(`Loading ${urls.length} images cancelled`); }
    )

    return {
      promise: imagesPromise,
      abort: () => {
        imagesRequests.forEach(image => image.abort());
        aborted = true;
        rejectPromise();
      }
    };
  }


  // To estimate if the file is downloading in normal time or should allow other files to download.
  // This estimation here is rather simple for real case scenario one could
  // make a more serious analysis of files downloaded earlier or use some library.
  function getDownloadSpeed() {
    const ESTIMATED_AVATAR_SIZE = 8000; // 1 KB (8000 b)
    const RESPONSE_TTFB = 35; // Time To First Byte from server (ms)

    const convertSpeed = (Mbs) => Mbs * 125  // Mb/s -> b/ms

    // Chrome and Opera support estimating current network capabilities according to recent activities in Mb/s
    // we can calculate how much time it will take to download one image in bach of 10 with speed converted to b/ms
    const downloadAvatarTime =
      navigator.connection && navigator.connection.downlink && (ESTIMATED_AVATAR_SIZE / convertSpeed(navigator.connection.downlink) + RESPONSE_TTFB);

    // Set timeout to either estimated download time or default for connection 10 Mb/s (1250 b/ms)
    return downloadAvatarTime  || (ESTIMATED_AVATAR_SIZE / convertSpeed(10)) + RESPONSE_TTFB;
  }

  const ESTIMATED_TIMED_OUT = getDownloadSpeed();

  /**
   * Given a single URL, return a deferred which is resolved once the image is
   * loaded or its loading has failed.
   *
   * For our purposes, a failed load is okay. If the load is successful, the
   * promise is resolved with an Image element.
   *
   * @param {String}  url
   * @return {Object} Contains a promise of the image, as well as an abort
   *                  method which should stop the image from loading.
   */
  function loadImage(url) {
    let rejectPromise;
    const controller = new AbortController();
    const signal = controller.signal;

    const imagePromise = new Promise((resolve, reject) => {
      const img = new Image();

      rejectPromise = reject;

      request = fetch(url, { signal })
        .then(response => {
          if(response.ok) {
            return response.blob();
          }
          throw new Error(response.status);
        })
        .then((blob) => {
          img.src = URL.createObjectURL(blob);
          return ({ result: img });
        })
        .catch(() => ({ result: null })) // no img means it failed, but that's okay, we just won't draw it.

        // in case of images of big sizes that exceed timeout do not wait for the download to finish
        // and let next batch of avatars to start downloading
        Promise.race([
          request,
          new Promise((timeoutResolve) =>
            setTimeout(() => timeoutResolve({ result: request}), ESTIMATED_TIMED_OUT)
          )
        ])
        .then(image => {
          resolve({ image });
        })  // image is either null, img or fetch request (for big files)
    });

    return {
      promise: imagePromise,
      abort: () => {
        controller.abort();
        rejectPromise('aborted');
      }
    }
  }

  // for some debugging messages
  function log(str) {
    const listEntry = document.createElement('li');
    const messageLog = document.getElementById('log');

    listEntry.textContent = str;

    messageLog.append(listEntry);
  }

  return loadImages;
})();
