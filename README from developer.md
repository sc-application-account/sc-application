## Hello

In the directories you can find my solutions, initial instructions from SoundCloud in `README.md` files and my comments in `notes.md` files.

For all solutions I have used JavaScript.

I have coded and tested my solutions on Ubuntu 19.04 with Chrome 74 and Firefox 68. Apart from mocha + chai framework for testing in aggregate task I haven't used any external libraries.

I have found one bug (perhaps it's purposeful omission) in waveform task, it is mentioned as a prelast bullet point in `notes.md`

In general the tasks were really fun and interesting to work with, I took a bit more time than suggested with queue task just because it was interesting to tinker with it.

Thanks and have a great day!
